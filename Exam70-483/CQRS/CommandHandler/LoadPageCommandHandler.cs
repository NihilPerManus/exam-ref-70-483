﻿using Melon.Core.CQRS.CommandHandler;
using Exam70_483.Container._Page;
using Exam70_483.Container.ContextButton;
using Exam70_483.CQRS.Command;
using Exam70_483.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam70_483.CQRS.CommandHandler
{
    public class LoadPageCommandHandler : ILoadPageCommandHandler
    {
        private IPageContainer _pageContainer;
        private IContextButtonContainer _buttonsContainer;


        public LoadPageCommandHandler(IPageContainer pageContainer, IContextButtonContainer buttonsContainer)
        {
            _pageContainer = pageContainer;
            _buttonsContainer = buttonsContainer;
        }


        public void Handle(ILoadPageCommand command)
        {
            //Set the page to the one requested. (This will fire an event to listeners to say "Hey, the page changed!")
            _pageContainer.Value = command.Page;

            //Check to see if the dataContext is one which contains context buttons
            if (_pageContainer.Value.DataContext is IContextualViewModel vm)
            {
                //Change the buttons to the viewmodel ones. This ensures that the buttons are consistent with the current view.
                _buttonsContainer.Value = vm.ContextButtons;
            }
        }
    }

    public interface ILoadPageCommandHandler : ICommandHandler<ILoadPageCommand>
    {
    }
}
