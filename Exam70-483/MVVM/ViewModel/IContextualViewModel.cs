﻿using Exam70_483.MVVM.Model.ContextButton;
using System.Collections;
using System.Collections.Generic;

namespace Exam70_483.MVVM.ViewModel
{
    public interface IContextualViewModel
    {
        //What buttons does this viewmodel confer to the side menu?
        IEnumerable<IContextButton> ContextButtons { get; }
    }
}