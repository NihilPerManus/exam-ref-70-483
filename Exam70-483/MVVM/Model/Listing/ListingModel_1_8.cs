﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_8 : BaseListingModel
    {
        public ListingModel_1_8(IHeaderContainer header) : base(header)
        {
            Name = "Starting a new task";
            GetListing("1-8");
        }

        public override void Main()
        {
            Task t = Task.Run(() => 
            {
                for (int i = 0; i < 10; i++)
                {
                    Write("*");
                }
            });

            t.Wait();
        }
    }
}
