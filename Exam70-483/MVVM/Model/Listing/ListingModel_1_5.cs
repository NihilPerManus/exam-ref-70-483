﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_5 : BaseListingModel
    {
        public ListingModel_1_5(IHeaderContainer header) : base(header)
        {
            Name = "Using the ThreadStaticAttribute";
            GetListing("1-5");
        }




        [ThreadStatic]
        static int _field;

        public override void Main()
        {
            new Thread(() => 
            {
                for (int x = 0; x < 10; x++)
                {
                    _field++;
                    WriteLine($"Thread A: {_field}");
                }
            }).Start();

            new Thread(() =>
            {
                for (int x = 0; x < 10; x++)
                {
                    _field++;
                    WriteLine($"Thread B: {_field}");
                }
            }).Start();

        }
    }
}
