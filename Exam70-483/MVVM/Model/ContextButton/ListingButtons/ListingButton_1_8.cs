﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam70_483.Container._Page;
using Exam70_483.MVVM.Model.Listing;
using Exam70_483.MVVM.View.Listing;
using Unity.Attributes;

namespace Exam70_483.MVVM.Model.ContextButton.ListingButtons
{
    public class ListingButton_1_8 : BaseListingButton
    {
        public ListingButton_1_8(IPageContainer pageContainer, ListingView listing, [Dependency("1-8")]IListingModel model) : base(pageContainer, listing, model)
        {
            Name = "Listing 1-8";
        }
    }
}
