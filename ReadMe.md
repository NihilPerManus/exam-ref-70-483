# Exam Ref 70-483
No this is not the exam or any of the questions contained within the exam, this is a tool which is used to run all the examples found in "Exam Ref 70-483: Programming in C#" by Wouter de Kort. 

I am writing this as a way of being able to organise all the examples into one application whilst also make it easy to refer to should it be used as a refresher.

## How to run

Open the solution in Visual Studio 2017 (Compatability in previous versions is unconfirmed) and click **Run** or press **[F5]**.

\* Alternatively, after building te project in Visual Studio the executable is compiled in the "Bin/Debug" folder, you can directly run that instead.


## How to contribute

Examples in this application are taken from the book **Programming in C# - Exam Ref 70-483** by Wouter De Kort

As it is not complete (the intial commit only included the first 5 listings) to contribute all you need to do is perform the following:


### Create a Listing Button

1. Create a Class named **ListingButton\_x\_y** (where x_y is the listing reference i.e ListingButton\_1\_5 for 1-5)

2. Inherit from **BaseListingButton**

3. Have Visual Studio Auto-Create the constructor when it complains. (Click on the line and press **[Ctrl]+[.]** to bring up the suggestions box, then press **[Enter]**)

4. In the constructor set the ***Name*** variable to the name of the listing **"Listing x-y"**
*(Example: Name = "Listing 1-5";)*

5. Add the attribute **[Dependency("x-y")]** to the **IListingModel** parameter in the constructor   


### Create a Listing Model
    
1. Create a Class named **ListingModel\_x\_y** (in the same manner as the button)

2. Inherit from **BaseListingModel**

3. Have Visual Studio Auto-Create the constructor and override fields

4. In the constructor type **GetListing("x-y");** - This will load the Listing Source explained later on

5. In the **Main()** this is where you fill the implementation details from the example in the booklet. *This code will run when the Execute button is pressed.*

6. Once complete, copy the implementation details ready to paste in the next step.

	
### Create Source Textfile

1. Right-click the **Listings** folder from the root directory in Visual Studio

2. Add a plain text file and rename it **x-y.txt**

3. Paste the source you copied in the previous step and format it appropriately.

4. In the **Properties** menu of the text file, set the **Build Action** to **Content**, and **Copy to Output Directory** to **Always**


### Register the components

1. Open the **CompositionRoot** Class found in the **_Composition** folder

2. Locate the **RegisterListings()** method and add your registration to the list in the appropriate order using **RegisterListing<ListingModel\_x\_y, ListingButton\_x\_y>("x-y");**


You should be able to run the application now and find your listing in the menu on the right-hand side. For now they are all in one list but this will be eventually categorised for easier searching.


If there are any issues in any of the above steps, it doesn't hurt to have a look at the initial classes that came with the solution, they provide decent examples on how each class should be structured.
 






