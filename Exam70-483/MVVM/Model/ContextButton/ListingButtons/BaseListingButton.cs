﻿using Exam70_483.Container._Page;
using Exam70_483.MVVM.Model.Listing;
using Exam70_483.MVVM.View.Listing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Exam70_483.MVVM.Model.ContextButton.ListingButtons
{
    public abstract class BaseListingButton : BaseContextButton, IListingButton
    {
        protected Page Page { get; set; }

        protected IPageContainer _pageContainer;
        protected IListingModel listingModel; 

        public BaseListingButton(IPageContainer pageContainer, ListingView listing, IListingModel model)
        {
            listingModel = model;
            Page = listing;
            _pageContainer = pageContainer;
            Icon = MaterialDesignThemes.Wpf.PackIconKind.Script;
        }

        public override ICommand ClickCommand => new Melon.Core.Relay.RelayCommand(e => 
        {
            Page.DataContext = listingModel; //Set the model to the current one
            _pageContainer.Value = Page;
            listingModel.OnShow(); //Trigger the page being made visible.
        });


    }

    public interface IListingButton : IContextButton
    {
    }

}
