﻿using Exam70_483.Container._Page;
using Exam70_483.Container.ContextButton;
using Exam70_483.Container.Header;
using Exam70_483.CQRS.Command;
using Exam70_483.CQRS.CommandHandler;
using Exam70_483.MVVM.Model.ContextButton.Home;
using Exam70_483.MVVM.Model.ContextButton.ListingButtons;
using Exam70_483.MVVM.Model.Listing;
using Exam70_483.MVVM.View;
using Exam70_483.MVVM.View.Listing;
using Exam70_483.MVVM.ViewModel;
using Exam70_483.MVVM.ViewModel.Content;
using Exam70_483.MVVM.ViewModel.Content.Default;
using Exam70_483.MVVM.ViewModel.Content.Listing;
using Exam70_483.MVVM.ViewModel.Header;
using Exam70_483.MVVM.ViewModel.Menu;
using Exam70_483.MVVM.ViewModel.Objectives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Injection;

namespace Exam70_483._Composition
{
    public class CompositionRoot
    {
        static IUnityContainer _container = new UnityContainer();

        [STAThread]
        public static void Main(string[] args)
        {

            Bootstrap(); //Bare registrations required for the application to work

            Inject(); //Custom registrations required for the application business to work

            Resolve(); //Configurations required prior to launching the application

            Launch(); //Launching the window with all the above configured 
        }

        private static void Resolve()
        {
            //Set the default view of the Pagecontainer button container if necessary
            var handler = _container.Resolve<ILoadPageCommandHandler>();
            handler.Handle(new LoadPageCommand() { Page = _container.Resolve<DefaultView>() });
        }

        private static void Launch()
        {
            App app = new App();

            MainWindow window = _container.Resolve<MainWindow>();

            app.Run(window);
        }

        private static void Inject()
        {
            _container

                //VIEWS
                .RegisterSingleton<DefaultView>().RegisterSingleton<IDefaultViewModel, DefaultViewModel>()
                .RegisterSingleton<ListingView>().RegisterSingleton<IListingViewModel, ListingViewModel>()

                //MODELS
               

                //COMMANDHANDLER
                .RegisterSingleton<ILoadPageCommandHandler, LoadPageCommandHandler>()


                //CONTAINERS
                .RegisterSingleton<IHeaderContainer, HeaderContainer>()
                .RegisterSingleton<IPageContainer, PageContainer>()
                .RegisterSingleton<IContextButtonContainer, ContextButtonsContainer>()
                ;

            RegisterListings();
        }

        private static void RegisterListings()
        {
            //Place the listings in order as it will be reflected in the menu list
            RegisterListing<ListingModel_1_1, ListingButton_1_1>("1-1");
            RegisterListing<ListingModel_1_2, ListingButton_1_2>("1-2");
            RegisterListing<ListingModel_1_3, ListingButton_1_3>("1-3");
            RegisterListing<ListingModel_1_4, ListingButton_1_4>("1-4");
            RegisterListing<ListingModel_1_5, ListingButton_1_5>("1-5");
            RegisterListing<ListingModel_1_6, ListingButton_1_6>("1-6");
            RegisterListing<ListingModel_1_7, ListingButton_1_7>("1-7");
            RegisterListing<ListingModel_1_8, ListingButton_1_8>("1-8");
            RegisterListing<ListingModel_1_9, ListingButton_1_9>("1-9");
            RegisterListing<ListingModel_1_10, ListingButton_1_10>("1-10");



            RegisterListing<ListingModel_1_14, ListingButton_1_14>("1-14");
            RegisterListing<ListingModel_1_15, ListingButton_1_15>("1-15");
        }

        private static void RegisterListing<TModel, TButton>(string name)
            where TButton : IListingButton
            where TModel : IListingModel
        {
            _container.RegisterSingleton<IListingModel, TModel>(name);
            _container.RegisterSingleton<IListingButton, TButton>(name);
        }

        private static void Bootstrap()
        {

            _container

                //VIEWMODELS
                .RegisterSingleton<IContentViewModel, ContentViewModel>()
                .RegisterSingleton<IMenuViewModel, MenuViewModel>()
                .RegisterSingleton<IHeaderViewModel, HeaderViewModel>()

                //VIEWS
                .RegisterSingleton<MainWindow>().RegisterSingleton<IMainWindowViewModel, MainWindowViewModel>();
        }
    }
}
