﻿using Melon.Core.Container;
using Exam70_483.MVVM.Model.ContextButton;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam70_483.Container.ContextButton
{
    public class ContextButtonsContainer : BaseContainer<IEnumerable<IContextButton>>, IContextButtonContainer
    {

    }

    public interface IContextButtonContainer : IContainer<IEnumerable<IContextButton>>
    {
    }
}
