﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    class ListingModel_1_4 : BaseListingModel
    {
        public ListingModel_1_4(IHeaderContainer header) : base(header)
        {
            Name = "Stopping a thread";
            GetListing("1-4");
        }

        public override void Main()
        {
            bool stopped = false;
            Thread t = new Thread(() =>
            {
                while (!stopped)
                {
                    WriteLine("Running...");
                    Thread.Sleep(1000);
                }
            });
            t.Start();
            ReadKey();
            stopped = true;
            t.Join();
        }
    }
}
