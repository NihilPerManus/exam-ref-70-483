﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    class ListingModel_1_7 : BaseListingModel
    {
        public ListingModel_1_7(IHeaderContainer header) : base(header)
        {
            Name = "Queuing some work to the thread pool";
            GetListing("1-7");
        }

        public override void Main()
        {
            ThreadPool.QueueUserWorkItem(s => 
            {
                WriteLine("Working on a thread from threadpool");
            });
        }
    }
}
