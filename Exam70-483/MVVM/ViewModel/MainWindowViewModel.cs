﻿using Exam70_483.MVVM.ViewModel.Content;
using Exam70_483.MVVM.ViewModel.Header;
using Exam70_483.MVVM.ViewModel.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam70_483.MVVM.ViewModel
{
    //This is the parent ViewModel which holds reference to other view models that form the different sections of the application
    public class MainWindowViewModel : IMainWindowViewModel
    {
        //This is the side menu 
        public IMenuViewModel SideMenuViewModel { get; set; }

        //This is responsible for showing the heading
        public IHeaderViewModel HeaderViewModel { get; set; }

        //This is responsible for showing the content of the current view. 
        public IContentViewModel ContentViewModel { get; set; }

        public MainWindowViewModel(IHeaderViewModel headerViewModel, IMenuViewModel menuViewModel, IContentViewModel contentViewModel)
        {
            HeaderViewModel = headerViewModel;
            SideMenuViewModel = menuViewModel;
            ContentViewModel = contentViewModel;
        }

    }

    public interface IMainWindowViewModel
    {
    }
}
