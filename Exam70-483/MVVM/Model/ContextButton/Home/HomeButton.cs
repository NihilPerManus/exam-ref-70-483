﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;
using Exam70_483.CQRS.Command;
using Exam70_483.CQRS.CommandHandler;
using Exam70_483.MVVM.View;

namespace Exam70_483.MVVM.Model.ContextButton.Home
{
    public class HomeButton : BaseContextButton, IHomeButton
    {
        private ILoadPageCommandHandler _loadPageCommandHandler;
        private ILoadPageCommand _loadPageCommand;


        public override ICommand ClickCommand => new Melon.Core.Relay.RelayCommand(e => _loadPageCommandHandler.Handle(_loadPageCommand));

        public HomeButton(ILoadPageCommandHandler loadPageCommandHandler, DefaultView defaultView)
        {
            Icon = PackIconKind.Home;
            //Load the page-load handler
            _loadPageCommandHandler = loadPageCommandHandler;

            //Load the command to change the page to the default view
            _loadPageCommand = new LoadPageCommand() { Page = defaultView };
        }

    }

    public interface IHomeButton : IContextButton
    {
    }
}
