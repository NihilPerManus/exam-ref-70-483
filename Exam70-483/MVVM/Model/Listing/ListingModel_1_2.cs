﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_2 : BaseListingModel
    {
        public ListingModel_1_2(IHeaderContainer header) : base(header)
        {
            GetListing("1-2");
            Name = "Using a background thread";
        }

        public override void Main()
        {
            Thread t = new Thread(new ThreadStart(ThreadMethod))
            {
                IsBackground = true
            };
            t.Start();
        }

        private void ThreadMethod()
        {
            for (int i = 0; i < 10; i++)
            {
                WriteLine($"ThreadProc:{i}");
                Thread.Sleep(1000);
            }
        }
    }
}
