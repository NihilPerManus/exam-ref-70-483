﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_6 : BaseListingModel
    {
        public ListingModel_1_6(IHeaderContainer header) : base(header)
        {
            Name = "Using ThreadLocal<T>";
            GetListing("1-6");
        }

        public static ThreadLocal<int> _field = new ThreadLocal<int>(() => 
        {
            return Thread.CurrentThread.ManagedThreadId;
        });

        public override void Main()
        {
            new Thread(() => 
            {
                for (int x = 0; x < _field.Value; x++)
                {
                    WriteLine($"Thread A: {x}");
                }
            }).Start();

            new Thread(() => 
            {
                for (int x = 0; x < _field.Value; x++)
                {
                    WriteLine($"Thread B: {x}");
                }
            }).Start();
        }
    }
}
