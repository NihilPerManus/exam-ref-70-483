﻿using Melon.Core.CQRS.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Exam70_483.CQRS.Command
{
    public class LoadPageCommand : ILoadPageCommand
    {
        //Provides a page to request we change to
        public Page Page { get; set; }
    }

    public interface ILoadPageCommand : ICommand
    {
        Page Page { get; set; }
    }
}
