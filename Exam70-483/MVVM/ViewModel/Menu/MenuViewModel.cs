﻿using Exam70_483.Container.ContextButton;
using Exam70_483.MVVM.Model.ContextButton;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam70_483.MVVM.ViewModel.Menu
{
    public class MenuViewModel : Melon.Core.Observable.ObservableClass, IMenuViewModel
    {
        //reference to the button container
        private IContextButtonContainer _contextButtonContainer;

        //Directly obtain the value of buttonslist in the referenced container
        public IEnumerable<IContextButton> ContextButtons { get { return _contextButtonContainer.Value; } set { _contextButtonContainer.Value = value; NotifyPropertyChanged(); } }

        public MenuViewModel(IContextButtonContainer contextButtonContainer)
        {
            //hold a reference to the button container
            _contextButtonContainer = contextButtonContainer;

            //Notify us when the menu buttons have changed
            _contextButtonContainer.PropertyChanged += (s, e) => NotifyPropertyChanged(nameof(ContextButtons));
        }

    }

    public interface IMenuViewModel
    {
        IEnumerable<IContextButton> ContextButtons { get; set; }
    }
}
