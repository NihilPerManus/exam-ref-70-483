﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_9 : BaseListingModel
    {
        public ListingModel_1_9(IHeaderContainer header) : base(header)
        {
            Name = "Using a Task that returns a value";
            GetListing("1-9");
        }

        public override void Main()
        {
            Task<int> t = Task.Run(() => 
            {
                return 42;
            });
            WriteLine(t.Result);
        }
    }
}
