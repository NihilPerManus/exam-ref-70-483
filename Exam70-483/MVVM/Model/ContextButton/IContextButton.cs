﻿using MaterialDesignThemes.Wpf;
using Melon.Core.Generic.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Exam70_483.MVVM.Model.ContextButton
{
    public interface IContextButton : IHasName
    {
        PackIconKind Icon { get; set; }
        ICommand ClickCommand { get; }
    }
}
