﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MaterialDesignThemes.Wpf;

namespace Exam70_483.MVVM.Model.ContextButton
{
    public abstract class BaseContextButton : IContextButton
    {
        public virtual PackIconKind Icon { get; set; }

        public abstract ICommand ClickCommand { get; }

        public string Name { get; set; }
    }
}
