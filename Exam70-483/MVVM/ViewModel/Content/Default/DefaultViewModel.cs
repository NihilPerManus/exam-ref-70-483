﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam70_483.Container.Header;
using Exam70_483.MVVM.Model.ContextButton;
using Exam70_483.MVVM.Model.ContextButton.Home;
using Exam70_483.MVVM.Model.ContextButton.ListingButtons;

namespace Exam70_483.MVVM.ViewModel.Content.Default
{
    public class DefaultViewModel : Melon.Core.Observable.ObservableClass, IDefaultViewModel

    {
        public DefaultViewModel(IHeaderContainer header, IEnumerable<IListingButton> listingButtons)
        {

            ContextButtons = listingButtons;

        }

        public IEnumerable<IContextButton> ContextButtons { get; }

    }

    public interface IDefaultViewModel : IContextualViewModel
    {
    }
}
