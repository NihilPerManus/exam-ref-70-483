﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_14 : BaseListingModel
    {
        public ListingModel_1_14(IHeaderContainer header) : base(header)
        {
            Name = "Using Task.WaitAll";
            GetListing("1-14");
        }

        public override void Main()
        {
            Task[] tasks = new Task[3];

            tasks[0] = Task.Run(() => 
            {
                Thread.Sleep(1000);
                WriteLine(1);
                return 1;
            });

            tasks[1] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                WriteLine(2);
                return 2;
            });

            tasks[2] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                WriteLine(3);
                return 3;
            });

            Task.WaitAll(tasks);

        }
    }
}
