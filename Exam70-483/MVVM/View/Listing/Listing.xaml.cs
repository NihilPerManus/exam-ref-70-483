﻿using Exam70_483.MVVM.ViewModel.Content.Listing;
using Exam70_483.MVVM.ViewModel.Objectives;
using System.Windows.Controls;

namespace Exam70_483.MVVM.View.Listing
{
    /// <summary>
    /// Interaction logic for Listing_1_1.xaml
    /// </summary>
    public partial class ListingView : Page
    {
        public ListingView(IListingViewModel vm)
        {
            DataContext = vm;
            InitializeComponent();
        }

    }
}
