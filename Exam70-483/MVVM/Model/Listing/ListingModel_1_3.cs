﻿using Exam70_483.Container.Header;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_3 : BaseListingModel
    {
        public ListingModel_1_3(IHeaderContainer header) : base(header)
        {
            GetListing("1-3");
            Name = "Using the ParameterizedThreadStart ";
        }

        public override void Main()
        {
            Thread t = new Thread(new ParameterizedThreadStart(ThreadMethod));
            t.Start(5);
            t.Join();
        }

        private void ThreadMethod(object obj)
        {
            for (int i = 0; i < (int)obj; i++)
            {
                WriteLine($"ThreadProc: {i}");
                Thread.Sleep(0);
            }
        }
    }
}
