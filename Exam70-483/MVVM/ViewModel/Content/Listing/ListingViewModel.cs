﻿using Exam70_483.MVVM.Model.Listing;
using Exam70_483.MVVM.ViewModel.Objectives;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Exam70_483.MVVM.ViewModel.Content.Listing
{
    public class ListingViewModel : Melon.Core.Observable.ObservableClass, IListingViewModel
    {
       
        private IListingModel _Listing;

        public string SourceText { get { return Listing.SourceText; } set { Listing.SourceText = value; NotifyPropertyChanged(); } }
        public string ConsoleText { get { return Listing.ConsoleText; } set { Listing.ConsoleText = value; NotifyPropertyChanged(); } }

        public IListingModel Listing { get { return _Listing; } set { _Listing.PropertyChanged -= _PropertyChanged; _Listing = value; _Listing.PropertyChanged += _PropertyChanged;  NotifyPropertyChanged(); } }

        private void _PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            NotifyPropertyChanged(nameof(ConsoleText));
        }

        public ICommand Execute { get => Listing?.Execute; }

    }

    public interface IListingViewModel : IExamViewModel
    {
    }

}



