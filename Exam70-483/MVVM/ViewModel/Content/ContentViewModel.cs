﻿using Exam70_483.Container._Page;
using Exam70_483.CQRS.Command;
using Exam70_483.CQRS.CommandHandler;
using Exam70_483.MVVM.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Exam70_483.MVVM.ViewModel.Content
{
    public class ContentViewModel : Melon.Core.Observable.ObservableClass, IContentViewModel
    {
        private IPageContainer _pageContainer;

        public Page Page { get { return _pageContainer.Value; } set { _pageContainer.Value = value; NotifyPropertyChanged(); } }

        public ContentViewModel(IPageContainer pageContainer, DefaultView defaultView, ILoadPageCommandHandler loadPageCommandHandler)
        {
            _pageContainer = pageContainer;

            //Notify us when the page changes
            _pageContainer.PropertyChanged += (s, e) => NotifyPropertyChanged(nameof(Page));

        }

    }

    public interface IContentViewModel
    {
    }
}
