﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_1 : BaseListingModel
    {
        public ListingModel_1_1(IHeaderContainer header) : base(header)
        {
            GetListing("1-1");
            Name = "Creating a thread with the Thread class";
        }

        private void ThreadMethod()
        {
            for (int i = 0; i < 10; i++)
            {
                WriteLine($"ThreadProc:{i}");
                Thread.Sleep(0);
            }
        }

        public override void Main()
        {
            Thread t = new Thread(new ThreadStart(ThreadMethod));
            t.Start();
            for (int i = 0; i < 4; i++)
            {
                WriteLine("Mainthread: Dosomework.");
                Thread.Sleep(0);
            }
            t.Join();
        }
    }
}
