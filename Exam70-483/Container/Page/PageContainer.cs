﻿using Melon.Core.Container;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Exam70_483.Container._Page
{
    public class PageContainer : BaseContainer<Page>, IPageContainer
    {

    }

    public interface IPageContainer : IContainer<Page>
    {
    }
}
