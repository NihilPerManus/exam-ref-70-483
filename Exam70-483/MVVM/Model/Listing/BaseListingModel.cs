﻿using Exam70_483.Container.Header;
using Melon.Core.Generic.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Exam70_483.MVVM.Model.Listing
{
    public abstract class BaseListingModel : Melon.Core.Observable.ObservableClass, IListingModel
    {
        private string _ConsoleText;
        private ICommand _Execute;
        private string _Name;
        protected IHeaderContainer _header;
        private bool waitingForKey;

        public string Name { get { return _Name; } set { _Name = value; NotifyPropertyChanged(); } }
        public string SourceText { get; set; }
        public ICommand Execute { get { return new Melon.Core.Relay.RelayCommand(e => { WriteLine("=== SCRIPT STARTED ==="); Task.Factory.StartNew(() => Main()); }); } set { _Execute = value; } }
        public ICommand KeyPressed { get { return new Melon.Core.Relay.RelayCommand(e => _KeyPressed()); } }

        private void _KeyPressed()
        {
            waitingForKey = false; //sets the thread to no longer waiting for a key to be pressed
        }

        public string ConsoleText { get { return _ConsoleText; } set { _ConsoleText = value; NotifyPropertyChanged(); } }

        public BaseListingModel(IHeaderContainer header)
        {
            _header = header;
        }


    public abstract void Main();
        protected void GetListing(string listing)
        {
            try
            {
                SourceText = File.ReadAllText(Directory.GetCurrentDirectory() + $@"\Listings\{listing}.txt");
            }
            catch
            {
                SourceText = "ERROR: Error retrieving source";
            }
        }
        
        protected void WriteLine(object obj)
        {
            Write(obj);
            Write("\n");
        }

        protected void Write(object obj)
        {
            ConsoleText += obj.ToString();
        }

        protected void ReadKey()
        {
            waitingForKey = true; //place application into awaiting Key response state
            do
            {
                Thread.Sleep(5); //Just adding a 5ms delay to reduce CPU usage 
            } while (waitingForKey);
            
        }

        public void OnShow()
        {
            _header.Value = Name;
        }
    }

    public interface IListingModel : IHasName, INotifyPropertyChanged
    {
       
        string ConsoleText { get; set; }
        string SourceText { get; set; }
        ICommand Execute { get; set; }

        void OnShow();
    }
}
