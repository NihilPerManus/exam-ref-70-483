﻿public override void Main()
{
    ThreadPool.QueueUserWorkItem(s => 
    {
        WriteLine("Working on a thread from threadpool");
    });
}