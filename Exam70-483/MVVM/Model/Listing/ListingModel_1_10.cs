﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam70_483.Container.Header;

namespace Exam70_483.MVVM.Model.Listing
{
    public class ListingModel_1_10 : BaseListingModel
    {
        public ListingModel_1_10(IHeaderContainer header) : base(header)
        {
            Name = "Adding a continuation (To a task)";
            GetListing("1-10");
        }

        public override void Main()
        {
            Task<int> t = Task.Run<int>(() => 42)
                .ContinueWith(i => i.Result * 2);

            WriteLine(t.Result);
        }
    }
}
