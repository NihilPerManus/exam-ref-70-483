﻿using Exam70_483.Container._Page;
using Exam70_483.MVVM.Model.Listing;
using Exam70_483.MVVM.View.Listing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Unity.Attributes;

namespace Exam70_483.MVVM.Model.ContextButton.ListingButtons
{
    public class ListingButton_1_1 : BaseListingButton 
    {        
        public ListingButton_1_1(ListingView listing, IPageContainer pageContainer, [Dependency("1-1")] IListingModel model) : base(pageContainer,listing,model)
        {
            Name = "Listing 1-1";

            
        }

       
    }

}
