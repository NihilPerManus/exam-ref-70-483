﻿using Melon.Core.Generic.Property;
using Exam70_483.Container.Header;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam70_483.MVVM.ViewModel.Header
{
    public class HeaderViewModel : Melon.Core.Observable.ObservableClass, IHeaderViewModel
    {
        private IHeaderContainer _headerContainer;

        //The heading to be displayed at the top. Pulls the value from the header container directly.
        public string Heading { get { return _headerContainer.Value; } set { _headerContainer.Value = value; } }

        public HeaderViewModel(IHeaderContainer headerContainer)
        {
            //hold reference to the header container
            _headerContainer = headerContainer;

            //Notify us when the heading changes
            _headerContainer.PropertyChanged += (s, e) => NotifyPropertyChanged(nameof(Heading));
        }

    }

    public interface IHeaderViewModel
    {
        string Heading { get; set; }
    }
}
