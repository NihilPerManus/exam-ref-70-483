﻿using System.Windows.Input;

namespace Exam70_483.MVVM.ViewModel.Objectives
{
    public interface IExamViewModel
    {
        string SourceText { get; set; }
        string ConsoleText { get; set; }
        ICommand Execute { get; }
    }
}