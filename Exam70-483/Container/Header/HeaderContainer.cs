﻿using Melon.Core.Container;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam70_483.Container.Header
{
    //This class is used to store the current heading
    public class HeaderContainer : BaseContainer<string>, IHeaderContainer
    {
    }

    public interface IHeaderContainer : IContainer<string>
    {
    }
}
